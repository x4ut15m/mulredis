import socket
import sys

if sys.version.startswith("2"):
    from lib.log import log2 as log
else:
    from lib.log import log


def getInputSocket(lport, pipe1, remoteIP):
    s = socket.socket(socket.AF_INET)
    s.bind(("0.0.0.0", lport))
    s.listen(5)

    conn, _ = s.accept()

    log.mlog.prin("\n[+] Connect from " + remoteIP)
    pipe1[remoteIP] = {
        "InputSocket": conn,
        "remoteIP": remoteIP,
        "InputPort": lport
    }



def getOutputSocket(lport, pipe2):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    port = lport + 3000
    s.bind(("0.0.0.0", port))
    s.listen(5)
    conn, _ = s.accept()

    pipe2["OutputSocket"] = conn
    # queue.put(connectInfo)
